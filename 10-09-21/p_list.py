colors = ['red','green','blue','magenta','black','white']

print(colors)
print(len(colors))

#Access specific element= Indexing
print(colors[0])
print(colors[-1])

index = 0
while index<len(colors):
    print(f"Index: {index} Value: {colors[index]}")
    index+=1