from django.contrib import admin
from myapp.models import Student, Contact

admin.site.site_header = "MyWebsite | Admin"

class ContactAdmin(admin.ModelAdmin):
    # fields = ['name','email']
    list_display = ['id','name','email','added_on','updated_on']
    list_filter=['name','added_on']
    list_editable = ['name']
    search_fields=['name','email']
    
admin.site.register(Student)
admin.site.register(Contact,ContactAdmin)
