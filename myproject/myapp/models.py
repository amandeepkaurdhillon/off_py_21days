from django.db import models
gen = (
    ('M','Male'),('F','Female'),('O','Others')
)
class Student(models.Model):
    name = models.CharField(max_length=50)
    email = models.EmailField(unique=True)
    contact = models.IntegerField(blank=True)
    dob = models.DateField()
    is_registered = models.BooleanField(default=True)
    gender = models.CharField(max_length=10,choices=gen)
    image = models.ImageField(upload_to='profiles/')
    address = models.TextField()
    registered_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
    
    class Meta:
        verbose_name_plural = "Student Table"

class Contact(models.Model):
    name = models.CharField(max_length=20)
    email = models.EmailField()
    subject = models.CharField(max_length=100)
    message = models.TextField()
    added_on = models.DateTimeField(auto_now_add=True)
    updated_on = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name 

    class Meta:
        verbose_name_plural = "Contact Table"