import random 
x = """
    Press 1: To create account
    Press 2: To check balance
    Press 3: To deposit 
    Press 4: To withdraw 
    Press 0: To exit
"""
print(x)
users=[]
def create_account():
    name = input("Enter Your Name: ")
    bal = float(input("Enter Initial Amount: "))
    pin = random.randint(1000,9999)
    cust = {'name':name, 'balance':bal,'pin':pin}
    users.append(cust)
    print(f"Dear {name}, Account created successfully, Pin Code:{pin}\n")

def check_balance():
    pin_code = int(input("Enter Pin Code: "))
    found=False
    for user in users:
        if user["pin"]==pin_code:
            found=True
            print(f"Welcome {user['name']}!!!")
            print(f"Current Balance: Rs.{user['balance']}/-\n")
    if found==False:
        print("Sorry You are not registered with us!\n")

def deposit():
    pin_code = int(input("Enter Pin Code: "))
    found=False
    for user in users:
        if user["pin"]==pin_code :
            found=True
            print(f"Welcome Back {user['name']}!")
            amt = float(input("Enter amount to deposit: "))
            user['balance'] = user['balance']+amt
            print(f"Your account credited with Rs.{amt}, Current Balance:Rs.{user['balance']}/-\n")
    if found==False:
        print("Sorry you are not registered with us!\n")

def withdraw():
    pin_code = int(input("Enter Pin Code: "))
    found=False
    for user in users:
        if user["pin"]==pin_code :
            found=True
            print(f"Welcome Back {user['name']}!")
            amt = float(input("Enter amount to withdraw: "))
            if amt>user['balance']:
                print("Insufficient balance!\n")
            else:
                user['balance'] = user['balance']-amt
                print(f"Your account debited with Rs.{amt}, Current Balance:Rs.{user['balance']}/-\n")
    if found==False:
        print("Sorry you are not registered with us!\n")
while True:
    option = input("Enter Your Choice: ")
    if option=="1":
        create_account()
    elif option=="2":
        check_balance()
    elif option=="3":
        deposit() 
    elif option =="4":
        withdraw() 
    elif option=="0":
        break
    else:
        print("Invalid Choice!\n")