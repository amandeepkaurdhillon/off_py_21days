print("Hello World")

x = 10
y = 20

print(f"Before: x={x}, y={y}")

#Swapping
x,y = y,x 

print(f"After: x={x}, y={y}")