class Circle:
    pi = 3.14
    def set_radius(self,r):
        self.radius = r
 
    def calc_area(self):
        print(f"r={self.radius} area={self.pi*self.radius**2}")


c1 = Circle()
c1.set_radius(10)
c2 = Circle()
c2.set_radius(7)

c2.calc_area()
c1.calc_area()
