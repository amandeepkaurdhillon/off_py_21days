#Parameterized
def add(x,y):
    return f"{x} + {y} = {x+y}"

#Positional arguments
print(add(10,3))
print(add(76,3))

#Keyword Arguments
print(add(y=38,x=9))

def Area(r=0,pi=3.14): #default arguments
    return f"pi={pi}, r={r}, area={pi*r**2}"

print(Area(10))
print(Area())
print(Area(20,2))
print(Area(8))