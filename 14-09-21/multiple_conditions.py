op="""
Press 1: To add User
Press 2: To view Users
Press 3: To remove User
Press 0: To exit
"""
print(op)
users = []
while True:
    choice = input("Enter Your Choice: ")
    if choice=="1":
        name = input("Enter User's name: ") 
        roll = input("Enter User's roll number: ")
        user = {'name':name, 'rno':roll}
        users.append(user)
        print(f"{name} added successfully!\n")
        
    elif choice=="2":
        print(f"Total Users: {len(users)}\n")
        for u in users:
            print("Name: ", u['name'],"Roll Number: ", u['rno'])
        print("-"*30)
           
        print("_"*10)
    elif choice=="3":
        rn = input("Enter User's roll number: ")
        match=False
        for user in users:
            if user['rno']==rn:
                users.remove(user)
                print(f"{user['name']} removed successfully!")
                match=True
            
        if match==False:
            print("User with this roll number not found")
    elif choice=="0":
        break 
    else:
        print("Invalid Choice!!!\n")