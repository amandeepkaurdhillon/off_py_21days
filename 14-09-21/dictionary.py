student = {
    'name':'Peter Parker',
    'rno':1,
    'subjects':['HTML','DBMS', 'DS'],
    'is_present':True,
    'tech':{
        'front-end':['HTML','CSS','Bootstrap'],
        'back-end':['Python','Django','SQLite'],
    }
}

# print(student)
#Access values
print(student['name'])
print(student['subjects'][1])
print(student['tech']['back-end'][2])

print("Before:", student,'\n')
#Add/update elements
student['age']=20
student['name'] = 'James'
student.update({'tech':'Full Stack', 'contact':8778888888})

#Remove elements
del student['subjects']
student.pop('rno')
student.popitem()

print("After: ", student)