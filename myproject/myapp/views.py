from django.shortcuts import render
from django.http import HttpResponse
from myapp.models import Contact

def first(request):
    return render(request,'index.html')

def second(request):
    return HttpResponse("<h1 style='color:green'>WELCOME TO MY WEBPAGE</h1>")

def contact_us(request):
    context={}
    # feedbacks = Contact.objects.all()[:3]
    # feedbacks = Contact.objects.all().order_by('-id')[:3]
    # feedbacks = Contact.objects.filter(name="Peter Parker")
    # feedbacks = Contact.objects.filter(name__contains="Parker")
    feedbacks = Contact.objects.all()
    context['fbs'] = feedbacks

    if "id" in request.GET:
        id_to_delete = request.GET.get('id')
        obj=Contact.objects.get(id=id_to_delete)
        obj.delete()
        context['status'] = "{} deleted successfully!".format(obj.name)

    if request.method=="POST":
        nm = request.POST.get('name')
        em = request.POST.get('email')
        sub = request.POST.get('subject')
        msz = request.POST.get('message')

        result = Contact(name=nm, email=em, subject=sub, message=msz)
        result.save()
        context["message"]="Dear {} Thanks for your feedback!".format(nm)
    
    return render(request,'contact.html', context)