a = 10 
b = 10.5 
c = '10' 
d = 1+5j 
e = [10,20,30]
f = (10,20,30) #immutable
g = {'name':'Peter','age':10}
h = {10,20,30,10,20,30}
i = None

print(type(a))
print(type(b))
print(type(c))
print(type(d))
print(type(e))
print(type(f))
print(type(g))
print(type(h))
print(type(i))